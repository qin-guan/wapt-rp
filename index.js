const http = require('http');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer({});

var server = http.createServer((req, res) => {
  proxy.web(req, res, { target: 'http://mercury.picoctf.net:64944' });
});

const port = process.env.PORT || 3001;

server.listen(port, "0.0.0.0", (err) => {
  if (err) console.error(err)
  else console.log(`Server running at ${port}`)
});
